## preamble
This applies to this model:
`ASUSTeK COMPUTER INC. ROG Zephyrus G14 GA401QM/GA401QM, BIOS GA401QM.408 05/13/2021`

> If your BIOS / Model differs from this please don't use the precompiled acpi_override and create your own.<br><br>How to do this you can get an idea [here](https://wiki.archlinux.org/title/DSDT#Recompiling_it_yourself). Just have a look below what I changed and reapply that to your DSDT tables.<br><br>I also got most inspiration from the [ThinkPad Yoga 3rd](https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Yoga_(Gen_3)#Enabling_S3_(before_BIOS_version_1.33)).

## what has changed

Line 535 in `dsdt.dsl`:<br>
`    Name (SS3, Zero)`

to<br>
`    Name (SS3, One)`

## how to create modified DSDT for your G14 variant

You can try using simple [script](scripts/modify-dsdt.sh)

## how to apply
- copy `acpi_override` to /boot/acpi_override
- add to grub:

on Arch:
```
GRUB_CMDLINE_LINUX_DEFAULT="mem_sleep_default=deep"
GRUB_EARLY_INITRD_LINUX_CUSTOM="/boot/acpi_override"
```

on Fedora:
```
GRUB_CMDLINE_LINUX="mem_sleep_default=deep"
GRUB_EARLY_INITRD_LINUX_CUSTOM="acpi_override"
```

- regenerate grub

### verify

```
$ cat /sys/power/mem_sleep
s2idle [deep]
```
